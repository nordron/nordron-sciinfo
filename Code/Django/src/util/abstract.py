# -*- coding: utf-8 -*-
import pdfx
import io
import os
import shutil

# Define the directory that articles inside
# shutil.rmtree("Article_abstract/")
HOST = os.uname()[1]
DATA_DIR = "/home/" + HOST + "/nordron-sciinfo/Code/Django/src/Articles_pdf/"
TXT_DIR = "/home/" + HOST + "/nordron-sciinfo/Code/Django/src/Articles_txt/"
ABS_DIR = "/home/" + HOST + "/nordron-sciinfo/Code/Django/src/Articles_abstract/"
# DATA_DIR = "/home/sciinfo/django_image/src/Articles_pdf/"
# ABS_DIR =  "/home/sciinfo/django_image/src/Articles_abstract/"


def initPath():
    if not os.path.exists('../Articles_abstract/'):
        os.mkdir('../Articles_abstract/')
    '''for i in os.listdir('Articles_abstract/'):
        os.remove('Articles_abstract/' + i)'''
    # os.remove('Articles_abstract/file_1abstract.txt')
    DATA_DIR = "../Articles_pdf/"
    pdf_name = []
    for filename in os.listdir(DATA_DIR):
        if not filename.replace(".pdf", ".txt") in os.listdir('../Articles_txt/'):
            print(filename)
            if(filename[-4:len(filename)].lower() == ".pdf"):
                pdf_name.append(filename)
                pdf = pdfx.PDFx("../Articles_pdf/" + filename)
                text = pdf.get_text()
                xml = io.open(
                    "../Articles_txt/" + filename.replace(".pdf", ".txt"), 'w', encoding='utf8')
                xml.write(text)
                xml.close()

# Abstract_start #
def abstract_start(a):

    arr = ["ABSTRACT", "A B S T R A C T"]

    c = 0
    for i in range(len(arr)):
        if(arr[i] in a.upper()):
            c = c+1

    if(c > 0):
        return 1
    else:
        return 0


def abstract_end(a):
    arr = ['INTRODUCTION', 'KEYWORDS', 'CATEGORI', 'INDEX']
    c = 0
    for i in range(len(arr)):
        if(arr[i] in a.upper()):
            c = c+1

    if(len(a.split()) == 0):
        c = c+1

    if(c > 0):
        return 1
    else:
        return 0
# Abstract_End #

def getAbstract(TXT_DIR, ABS_DIR):
    for filename in os.listdir(TXT_DIR):
        if not filename.replace(".txt", "abstract.txt") in os.listdir(ABS_DIR):
            myfile = io.open(TXT_DIR + filename, encoding='utf8')
            a1 = []

            c = 0
            print(myfile)
            for i in myfile.readlines():

                if(abstract_start(i)):
                    c += 1
                elif((abstract_end(i) or abstract_end(i.strip()[0:8])) and c > 0):
                    c = 0
                    if(len(a1) < 10 and c == 0):
                        c = c+1

                if c > 0:
                    a1.append(i)

            ab = io.open(ABS_DIR + filename.replace(".txt",
                                                    "abstract.txt"), 'w', encoding='utf8')
            print(ab)
            for i in range(len(a1)):
                ab.write(a1[i])
            ab.close()


if __name__ == "__main__":
    getAbstract(TXT_DIR, ABS_DIR)
