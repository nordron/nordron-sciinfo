import React from 'react'
import ReactDOM from 'react-dom'
import './query.sass'
import spelt from 'spelt/dist/index.js';
// import one of the dictionaries
import {dictionary} from "spelt-us-dict";
import keyword_extractor from 'keyword-extractor'
import articles from './articles.js'
import TagCloud from 'react-tag-cloud';
import randomColor from 'randomcolor';
//import wikipedia from 'wikipedia-js'

// build dictionary
const check = spelt({
	dictionary:dictionary,
	// can be either "gb" or "us"
	distanceThreshold:0.2
	// when a correction found with this distance
	// we'll stop looking for another
	// this would improve performance
})

class InputBox extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			spellCheckList: [],
			query: this.props.defaultQuery || '',
			activeIndex: -1,
			keywords: [],
			articles: [],
			showMsg: false
		}
		this.inputHandler = this.inputHandler.bind(this)
		this.keyHandler = this.keyHandler.bind(this)
		this.keyUpHandler = this.keyUpHandler.bind(this)
		this.clickParagraph = this.clickParagraph.bind(this)
	}
	componentWillMount() {
		/*
		fetch('https://en.wikipedia.org/w/api.php?action=opensearch&search=machine learning&limit=1&namespace=0&format=jsonfm', {method: 'get',mode: 'no-cors'})
		.then(function(response) {
		    console.log(response)
		}).catch(function(err) {
		    // Error :(
		})
		var sentence = "President Obama woke up Monday facing a Congressional defeat that many in both parties believed could hobble his presidency."
 
		//  Extract the keywords
	
		
		
		var extraction_result = keyword_extractor.extract(sentence,{
		                                                                language:"english",
		                                                                remove_digits: true,
		                                                                return_changed_case:true,
		                                                                remove_duplicates: false
 																	})
		console.log(extraction_result)
		*/
		this.setState({
			articles: articles.map(a=>{
				a.active = false
				a.keywordIndex = 0
				a.paragraph = ''
				return a
			})
		})
	}
	render() {
		console.log('state query: ' + this.state.query)
		let wrongCount = 0
		let stringLength = 5 // left default position is 5px
		const corrections = this.state.spellCheckList.map( (c,qIndex) => {
			if ( !c.correct ) wrongCount += 1
			if ( qIndex > 0 ) stringLength += (this.state.spellCheckList[qIndex-1].raw.length*6 + 6)
			let list = c.corrections.map( (word,index) => {
				if ( index < 5) {
					let className = 'correction-word'
					if ( qIndex === this.state.spellCheckList.length - 1 && index === this.state.activeIndex ) {
						className = 'correction-word active'
					}
					return (
						<div 
							className={className}
							onClick={this.correctQuery.bind(this,qIndex,word.correction)}
						>{word.correction}</div>
					)
				}
			})
			return (
				<div className="corrections-list" style={{left: `${stringLength}px`}}> {list} </div>
			)
		})
		const wrongMsg = (
				<div id="msg-alert">
					You had better go back to learn the English
				</div>
			)
		let keywords = ""
		this.state.keywords.forEach( (k,index) => {
			keywords += index < this.state.keywords.length - 1 ? ( k + " ") : k 
		})
		/*
		const articlesDom = this.state.articles.map( (a,i)=>{
			let keyword = a.keywords[a.keywordIndex].keyword
			console.log(keyword)
			let sentence = a.keywords[a.keywordIndex].paragraph[0]
			let indexOfKeyWord = sentence.indexOf(keyword)
			let paragraph1 = sentence.slice(0,indexOfKeyWord)
			let paragraph2 = sentence.slice(indexOfKeyWord + keyword.length, sentence.length)
			// console.log('indexOfKeyWord: ' + indexOfKeyWord)
			// console.log('paragraph1: ' + paragraph1)
			// console.log('paragraph2: ' + paragraph2)
			// console.log('kIndex: ' + kIndex)
			// console.log('keywordIndex: ' + a.keywordIndex)
			const paragraph = (
					<div className={ a.active ? "paragraph active" : "paragraph"}>
						<span>{paragraph1}</span>
						<code>{keyword}</code>
						<span>{paragraph2}</span>
					</div>
				)
			const keywords = a.keywords.map( (k,kIndex) => {
				let className = ''
				if ( a.active && kIndex === a.keywordIndex ) className = 'keyword active'
				else className = 'keyword'
				return (
					<span 
						className= {className}
						onClick={ () => this.clickParagraph(true,i,kIndex)}
						>
						{k.keyword}
					</span>
				)
			})

			return (
				<div 
					className="article"
					>
					<span  
						className="title"
						onClick={ () => this.clickParagraph(false,i,0)}
						>
						{a.title}
					</span>
					<span 
						className="content"
						onClick={ () => this.clickParagraph(false,i,0)}
						>
						{a.abstract.length > 200 ? (a.abstract.substring(0,200) + '...') : a.abstract}
					</span>
					<div className="article-keywords">
						{keywords}
					</div>
					{paragraph}
				</div>
			)
		})*/
		if ( wrongCount >= 5 ) window.location.href = 'https://learnenglishkids.britishcouncil.org/zh-hant'
		return (
			<div id="query-box">
				<textarea
					id="q"
					className="form-control"
					rows="5"
					name="q" 
					placeholder="enter the keyword"
					value={this.state.query}
					onChange={this.inputHandler}
					onKeyDown={this.keyHandler}
					onKeyUp={this.keyUpHandler}
				>
				</textarea>
				<input 
					id="sentence-keywords"
					name="keywords"
					value={keywords}
					style={{opacity: 0, position: "absolute", height: 0, width: 0}}
				/>
				<div className="results-container">
					{corrections}
				</div>
			</div>
		)
	}
	inputHandler(e) {
		if ( this.state.activeIndex < 0 ) {
			this.setState({
				query: e.target.value
			})
			//fumc()
		
			const text = e.target.value.split('')
			const lastString = text[text.length - 1]
			if ( lastString === ' ' ) {
				//console.log(check(e.target.value.split(' ')[0]))
				let keywords = e.target.value.split(' ')
				keywords.pop(keywords.length-1) //remove the last element
				const results = keywords.map( (word,index) => {
					return check(word)
				})
				this.setState({
					spellCheckList: results
				})
				const sentence = this.state.query
				const extraction_result = keyword_extractor.extract(sentence,{
		                                                                language:"english",
		                                                                remove_digits: true,
		                                                                return_changed_case:true,
		                                                                remove_duplicates: false
 																	})
				this.setState({
					keywords: extraction_result
				})
			}
		}
	}
	correctQuery(index,correction) {
		let query = this.state.query.split(" ")
		query[index] = correction
		query.pop(query.length - 1)
		let newQuery = ''
		query.forEach( (q,i) => {
			newQuery += (q + ' ') 
		})
		this.state.spellCheckList.pop(index)
		const spellCheckList = this.state.spellCheckList
		this.setState({
			query: newQuery,
			spellCheckList: spellCheckList
		})
	}
	keyHandler(e) {
		//console.log(e.keyCode)
		if( e.keyCode === 40 ) {
			let nowActiveIndex = this.state.activeIndex + 1
			//maker sure the similar words exits
			const lastCorrectionIndex = this.state.spellCheckList.length - 1
				//console.log(this.state.spellCheckList[lastCorrectionIndex])
			const corrections = this.state.spellCheckList[lastCorrectionIndex].corrections
			if ( corrections.length > 0 ) {
				let maxCorrections =  corrections.length > 5 ? 5 : corrections.length
				this.setState({
					activeIndex: nowActiveIndex > maxCorrections - 1 ? maxCorrections - 1 : nowActiveIndex 
				})
			}
		}
		if( e.keyCode === 38 ) {
			let nowActiveIndex = this.state.activeIndex - 1
			this.setState({
				activeIndex: nowActiveIndex < 0 ? 0 : nowActiveIndex 
			})
		}
		if ( e.keyCode === 13 ) {
			if ( this.state.activeIndex >= 0 ) {
				const lastCorrectionIndex = this.state.spellCheckList.length - 1
				//console.log(this.state.spellCheckList[lastCorrectionIndex])
				const correction = this.state.spellCheckList[lastCorrectionIndex].corrections[this.state.activeIndex].correction
				this.correctQuery(lastCorrectionIndex,correction)
			}
		}
	}
	keyUpHandler(e) {
		if ( e.keyCode === 13 && this.state.activeIndex >= 0 ) {
			this.setState({
				activeIndex: -1
			})
		}
	}
	clickParagraph(status,i,kIndex) {
		console.log('now kIndex: ' + kIndex)
		console.log('paragraph: ' + this.state.articles[i].keywords[kIndex].paragraph[0])
		let articles = this.state.articles
		articles[i].active = status
		articles[i].keywordIndex = kIndex
		this.setState({
			articles: articles
		})
	}
}

export default InputBox
/*
const MyCloud = (props) => {
	const keywords = ['deep learnining', 'machine learning','professor', 'project']
	const counter = [4.0,7.0,1.0,2.0]
	const wiki = ['ddddddddddddddddddddd', 'dfsfsdfffffffffffffffffff','123','34555']
	const k = keywords.map( (i,index) => {
		const fontS = 20 + 3*counter[index]
		return (
			<a href={"https://en.wikipedia.org/wiki/"+i}>
				<div 
					style={{fontSize: fontS}}
					onClick={()=>console.log(wiki[index])}
					>
						{i}
				</div>
			</a>
		)
	})

    return (
      <TagCloud 
        style={{
          fontFamily: 'sans-serif',
          fontSize: 20,
          fontWeight: 'bold',
          fontStyle: 'italic',
          color: () => randomColor(),
          padding: 5,
          width: '500px',
          height: '500px'
        }}>
        {k}
      </TagCloud>
    )
}
*/