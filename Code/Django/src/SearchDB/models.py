# -*- coding: utf-8 -*-
"""=========================================================================================
 Purpose: set or define the database and the model for the web
 Reference: https://docs.djangoproject.com/en/2.0/topics/db/models/
 Relative file: views.py
========================================================================================="""

from __future__ import unicode_literals
from django.db import models
from .validators import validate_file_extension

# Create your models here.


class Article(models.Model):

    # Define number of columns for this table
    # Fuction Reference: https://docs.djangoproject.com/en/2.0/ref/models/fields/#model-field-types
    filename = models.CharField(max_length=255)
    content = models.TextField()
    title = models.CharField(max_length=255)
    # year = models.IntegerField()
    # year = models.IntegerField(default=0)
    doi = models.URLField(max_length=128, default='http://dx.doi.org/')
    # upload = models.FileField(upload_to='Articles_pdf/')

    def __str__(self):

        # Define the model name
        return self.filename


class Document(models.Model):
    docfile = models.FileField(upload_to='Articles_pdf_temp/', validators=[
                               validate_file_extension])
