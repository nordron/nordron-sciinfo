import csv
import numpy as np
import matplotlib.pyplot as plt
import sys
import re

# %%  Load csv files into dictionary in list
def Load_csv(filename, chart='pie'):
    dic_list = []
    with open(filename, 'r') as csvfile:
        reader = csvreader(csvfile)
        data = np.array(list(reader))
        if chart == 'pie':
            for i in range(1,data.shape[1]-1):
                unique, counts = np.unique(data[1:,i], return_counts=True)
                dic = dict(zip(unique,counts))
                dic['Question'] = data[0,i]
                dic_list.append(dic)
            return dic_list
        elif chart == 'histogram':
            dic = {}
            for i in range(1,data.shape[0]):
                data[i,1:-1] = np.array([int(re.findall(r'\d+',x)[0]) for x in data[i,1:-1]])
            return data

# %% Save the pie chart into image files
def plot_csv(data_list):
    plt.clf()
    plt.close()
    for i in range(len(data_list)):
        fig = plt.figure()
        labels = []
        sizes = []
        for j in data_list[i]:
            if j is not 'Question' and j:
                labels.append(j)
                sizes.append(data_list[i][j])
        plt.pie(sizes, labels=labels, autopct='%1.1f%%')
        plt.title(data_list[i]['Question'])
        plt.axis('equal')
        plt.savefig('Question' + str(i+1) + '.png')

# %% Save the histogram into image files
def plot_histogram(data_combined, student_name, left=-45, right=105):
    plt.clf()
    plt.close()
    data = np.zeros([len(student_name),data_combined.shape[1]-2])
    for i in student_name:
        p = np.where(data_combined == i)
        j = data_combined[p[0],1:-1]
        j = j.astype(int)
        data[student_name.index(i),:] = j[1,:] - j[0,:]
    for i in range(data_combined.shape[1]-2):
        fig = plt.figure()
        plt.hist(data[:,i], bins=0.1*(right-left), edgecolor='black', linewidth=1.2, align='mid', range=(left, right))
        mean = np.mean(data[:,i])
        median = np.median(data[:,i])
        print('Mean and median of Q%d is (%f,%f)'%(i+1,mean,median))
        plt.xlabel('Growth of score')
        plt.ylabel('Counts')
        plt.savefig('Histogram_Q' + str(i+1) + '.png', bbox_inches='tight')
        plt.close(fig)

# %% Main part
if len(sys.argv) <= 1:
    exit("Input file missing")
else:
    plot_csv(Load_csv(sys.argv[1]))
    plot_histogram(Load_csv(sys.argv[1],'histogram'))
