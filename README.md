﻿### Deprecated packages and Errors ###

Packages:

1. stop-words (Not maintained since 2018)
1. pyPdf (Not maintained since 2010), newest version is pyPdf4 (Not maintained since 2018)
1. pdfx (Not maintained since 2016)
1. bs4 (Not maintained since 2016)
1. rake-nltk (Not maintained since 2018)
1. pdfrw (Not maintained since 2017)

Errors:

1. Python 2.7 docker image is deprecated.
1. Python packages in requirements.pip are missing its version number. Pip ended up installing the latest packages and errors raised because newer packages need python 3.

### System requirements for docker ###

* Linux kernel version 3.10 or higher
* 2.0 GB of RAM
* 3.0 GB disk space available
* A static IP address

## The following steps are for linux (RHEL) users, if you're using Windows or Mac OS, refer to the next part##

### Set up git ###

* Step 1: Check if Git has already been installed in CentOS computer.

		git --version

	If not, install Git in CentOS computer. (For other OS, the command yum might change, e.g. apt)

		sudo yum install git
	
	To confirm that these configurations were added successfully
		
		git config --list


* Step 2: Clone the repository.

	First move to the ~ directory.

		cd ~

	Then clone the repository.

		git clone https://bitbucket.org/nordron/nordron-sciinfo
		
* Step 3: Move into the repository.

		cd nordron-sciinfo

* Step 4: Pull the files.

		git pull
		
### Install pip ###

		sudo yum update
		sudo yum install python-pip
		sudo yum install epel-release		
	
### Convert pdfs into txts ###

* Step 1: Install pdfx.

		sudo pip install pdfx
		
* Step 2: Make directories called **Articles_pdf** and **Articles_txt** under Django/src.

		mkdir ~/nordron-sciinfo/Code/Django/src/Articles_txt ~/nordron-sciinfo/Code/Django/src/Articles_pdf/
		
* Step 3: Move into the directory **Articles_pdf**.
		
		cd ~/nordron-sciinfo/Code/Django/src/Articles_pdf/
		
	Check the files:
	
		ls		

* Step 4: Add pdf files to **Articles_pdf** folder, you can use either FileZilla or scp command

* Step 5: Convert the pdf files to txt files.
	*1.write a batch file and run it.
	
		$~/nordron-sciinfo/Code/Django/src/util
		$ source pdf2txt.sh
	 
	*2.Or run the pdftxt.py that transfer from pdf to txt, count the words of article, and other postprocess at the same time.
	(only run after docker-compose build.)
	
		$ cd ~/nordron-sciinfo/Code/Django/src/SearchDB
		$ python pdf2txt.py
		
	make sure the follow code is in the main function.

		if __name__ == "__main__":
			# --------Test - transfer to txt + count the word ---
    		txts = pdf2txt()
    		wordNum = countWordNumber([], TXT_DIR)


* Step 6: Convert the txt files to abstract files.
		
	Move into the directory 'util'.(Note: if your director is on another drive, you must use (cd <path to this repository>\Code\Django\src\util)
	
	or copy the abstract.py file to the 'src' directory.
	
		Use python3 to Type(python3 abstract.py) in linux system to convert the txt files to abstract files. 

	
	
### Install Django with Postgres, Nginx and Gunicorn in docker ###
		
* Step 1: Install Docker Engine
	
	Check if Docker has already been installed

		docker info

	if not install docker engine

		sudo yum -y install docker docker-registry
		sudo yum install docker.io
		sudo systemctl start docker

* Step 2: Install docker-compose

		sudo pip install docker-compose


* Step 3: Configure the port with

		vim <Django directory>/docker-compose.yml
		
	find the following lines
	
		  nginx:
			image: nginx:latest
			container_name: ng02
			ports:
			  - "9000:7000"
			volumes:
			  - ./src:/src
			  - ./config/nginx:/etc/nginx/conf.d
			depends_on:
			  - web
			  
	then change "9000:7000" into "<port you want>:7000"
	e.g. "2345:7000"

* Step 4: For the first time setting up, run the following command to build up docker in your computer

		sudo docker-compose build

	Wait for a while before it finishes collecting the required packages. If error occurs check with 'ls' if all files have the same color. If not, check with 'ls -l <file>' the permission of the files. Change files with different permissions so that the user is added. Do this with 'chmod 666 <file>' (or 'chmod a+rwx <file>')

* Step 5: Save the file and run (If you run CentOS 7 and encounter a permission problem run 'setstatus' to check the current mode and validate if this an SELinux problem. If the current mode is 'disabled' run 'setenforce 0' to change to 'permissive' before the next command)

		sudo docker-compose up 
	or `sudo docker-compose up -depends_on`

	if the above command is not working, use

        sudo docker-compose up -d

	instead.


* Step 6: Check your website at "localhost:<port you set>". For server version, enter "<your static IP address>:<port you set>".

* Step 7: Enter "localhost:<port you set>/update" to synchronize your database. For server version, change localhost to IP as Step 6 did.

* Step 8: If you make some change of this project in your computer, e.g. design of webpage, run the following command to restart docker in your computer

		sh <Django directory>/restartDocker.sh
		
* Step 9 (Optional): If you want to run docker without sudo, you can manage docker as a non-root user. First you add docker to a group: `sudo groupadd docker` and then you add you yourselve as user to that group: `sudo gpasswd -a $USER docker`. You can use `docker run hello-world` to check if you can run docker without sudo.
		
		
## For Windows and Mac OS users ##

### Set up git ###

* Step 1: Install git client software, you can choose either [SourceTree](https://www.sourcetreeapp.com/) or [Git bash](https://git-scm.com/downloads).

* Step 2: Clone this repository (Don't know how? In the top right corner of this page you can see a "Clone wiki" button, press it.)

* Step 3: Learn how to use git by doing the 15 minuts interactive git tutorial here: [15 Minutes GIT tutorial](https://try.github.io/levels/1/challenges/1) AND by checking out [this page](https://bitbucket.org/temn/nordlinglab-web/wiki/git_and_ssh) under "Command Line Session":  

### Set up pip ###

* Step 1: Install [Python](https://www.python.org/downloads/)

* Step 2: Open command line (Windows - [here a list of commands](https://www.lifewire.com/list-of-command-prompt-commands-4092302))/ terminal (Mac) and type ```pip``` ([difference between Unix and Win commands](https://www.lemoda.net/windows/windows2unix/windows2unix.html))

* Step 3: If ```pip``` cannot work, you need to set the environment variable.

### Convert pdfs into txts ###

* Step 1: Install pdfx.

		pip install pdfx
		
* Step 2: Make directories called **Articles_pdf** and **Articles_txt** under Django/src. (Note: if your directory is on another drive than C:, use e.g. e:\<path to folder>)

		mkdir <path to this repository>\Code\Django\src\Articles_txt <path to this repository>\Code\Django\src\Articles_pdf\
		
* Step 3: Move into the directory **Articles_pdf**. (Note: if your director is on another drive, use cd /d d:\<path to directory>\Code\Django\src\Articles_pdf\)
		
		cd <path to this repository>\Code\Django\src\Articles_pdf\
		
	Check the files:
	
		dir

* Step 4: Add pdf files to **Articles_pdf** folder, and you need to add at least two pdf files. you can use either FileZilla or scp command. If you're using local computer, just do copy and paste

* Step 5: Convert the pdf files to txt files.
		
	$ cd ~/nordron-sciinfo/Code/Django/src/SearchDB
		$ python pdf2txt.py
		
	Run the pdftxt.py and make sure the follow code is in the main function.

		if __name__ == "__main__":
			# --------Test - transfer to txt + count the word ---
    		txts = pdf2txt()
    		wordNum = countWordNumber([], TXT_DIR)

	or write a batch file with the following code and run it ($ source pdf2txt.sh) in directory ~/nordron-sciinfo/Code/Django/src/util.
		
		
* Step 6: Convert the txt files to abstract files.

		Move into the directory util.(Note: if your director is on another drive, you must use (cd <path to this repository>\Code\Django\src\util)
		Use python3 to Type(python abstract_window.py) for window system, or use python3 to Type(python3 abstract.py) in linux system to convert the txt files to abstract files. 
		Then you will find the abstract.txt in Articles_abstract folder.  


### Set up docker ###

* Step 1: Install [Docker toolbox for Windows](https://docs.docker.com/toolbox/toolbox_install_windows/)/[Docker toolbox for Mac OS](https://docs.docker.com/toolbox/toolbox_install_mac/). If you cannot setup Docker toolbox successfully, please check your virtual machine's version whether it is the newest one.
	
	For newer versions of Windows 10 (Pro versions or Creator's Update) do as follows:
	
	a) Install [Docker Community Edition](https://www.docker.com/community-edition) (not Docker Toolbox)
	
	b) Enable Hyper-V by Open Control Panel -> System and Security -> Programs (left panel) -> Turn Windows features on or off -> Check the Hyper-V box. If you don't have Hyper-V option, then your Windows OS is not Enterprise Edition. Also, Docker seems to work a lot better on Intel processors (not AMD).
	
	c) Add C:\Program Files\Docker\Docker\resources\bin to Path in Environmental Variables. To do so, in Search (Windows Button), search for and then select: System (Control Panel), click the Advanced system settings link. Click Environment Variables. In the section System Variables, find the PATH environment variable and select it. Click Edit. If the PATH environment variable does not exist, click New. In the Edit System Variable (or New System Variable) window, specify the value of the PATH environment variable. Click OK. Close all remaining windows by clicking OK.
	
	d) IMPORTANT: get sure you share your drives. In the task bar, right click on the docker icon (if it isn't there, it means docker service is not running and you should start from step 1 again) and select settings. In the settings window select on the left side "Shared Drives", select the drive on which you downloaded the git files, type in your Windows password to confirm.
	
	e) IGNORE step 2 and 3, just do step 4, 5 and 6, then go to your browser (Firefox, Chrome) and type "localhost:YOUR_IP_FROM_STEP_4" (without the "") and voil�, you should be done. (no need for step 7 and 8)
	
* Step 2: Launch Docker toolbox and wait until it finish setting up

* Step 3: Type the command ```docker-machine ip``` to get the ip of Docker.

* Step 4: Configure the port with

		vim <Django directory>/docker-compose.yml
		
	find the following lines
	
		  nginx:
			image: nginx:latest
			container_name: ng02
			ports:
			  - "9000:7000"
			volumes:
			  - ./src:/src
			  - ./config/nginx:/etc/nginx/conf.d
			depends_on:
			  - web
			  
	then change "9000:7000" into "<port you want>:7000"
	e.g. "2345:7000"

* Step 5: For the first time setting up, run the following command to build up docker in your computer

		docker-compose build

	Wait for a while before it finishes collecting the required packages.

* Step 6: Save the file and run

		docker-compose up

	if the above command works well, after restarting you can also use

        docker-compose up -d

	instead.
	
	Note that ```-d``` is to make it run in background.
	
* Step 7: Check your website at "<ip for docker-machine>:<port you set>"

* Step 8: Enter "<ip for docker-machine>:<port you set>/update" to synchronize the database.
