import React from 'react'
import './main.sass'


class Main extends React.Component{
	constructor(props) {
		super(props)
		this.state = {
			//clustersTag: this.props.clusters[this.props.clusters.length-1].subNodes,
			clusterTagsLayer1: this.props.clusterTags || [],
			clusterTagsLayer2: [],
			articles: this.props.articles,
			clusters: this.props.clusters,
			currentTags: [],
			layer1Index: 0,
			layer2Index: -1
			//currentClusterIndex: this.props.clusters.length-1
		}
		this.changeCluster = this.changeCluster.bind(this)
	}
	render() {
		let articlesDom = []
		let clusterTagsDom = null
		let clusterSubTags = []
		console.log('currentTags: ', this.state.currentTags)
		if ( this.state.articles.length > 0 ) {
			articlesDom = this.state.articles.map( a => {
				if ( this.state.currentTags.length > 0 ) {
					console.log('hi')
					let fit = true
					for ( let i = 0 ; i < this.state.currentTags.length ; i++ ) {
						console.log('a tag: ', a.tags[i])
						console.log('currentTags: ', this.state.currentTags[i])
						if ( a.tags[i] !== this.state.currentTags[i] ) {
							fit = false
							break
						}
					}
					if ( fit ) return <Article data={a}/>
				}
				else return <Article data={a}/>
			})
		}
		if ( this.state.clusterTagsLayer1.length > 0 ) {
			clusterTagsDom = this.state.clusterTagsLayer1.map( (i,index) => {
					return (
						<div 
							className={ this.state.layer1Index === index + 1 ? "cluster-tag active":"cluster-tag"}
							onClick={()=>{
								this.setState({
									currentTags: [i.name],
									clusterTagsLayer2: i.subtags,
									layer1Index: index+1, // beacuse index 0 is "ALL" tag
									layer2Index: -1
								})
							}}
						>
							{i.name}
						</div>
					)
				}
			)
		}
		if ( this.state.clusterTagsLayer2.length > 0 ) {
			clusterSubTags = this.state.clusterTagsLayer2.map( (i,index) => {
				return (
					<div
						className={ this.state.layer2Index === index ? "subtag sub-active":"subtag"}
						onClick={ ()=>{
							let currentTags = this.state.currentTags
							currentTags = [currentTags[0], i]
							this.setState({
								currentTags: currentTags,
								layer2Index: index
							})
						}}
					>
					{i}
					</div>
				)
			})
		}
		return (
			<div id="main-page">
				<img 
					src="http://www.londoncleaningcarpets.co.uk//images/layout/close-btn.png" 
					className="close-icon"
					onClick={ ()=>{
						let DOM = document.getElementById('cluster-page')
						DOM.style.display = 'none'
					}}
					/>
				<div className="cluster-tags layer1">
					<div 
						className={ this.state.layer1Index === 0 ? "cluster-tag active":"cluster-tag"}
						onClick={()=>{
							this.setState({
								currentTags: [],
								clusterTagsLayer2: [],
								layer1Index: 0,
								layer2Index: -1
							})
						}}
					>
						All
					</div>
					{ clusterTagsDom }
				</div>
				{ clusterSubTags.length !== 0 && (<div className="cluster-tags layer2">
					{ clusterSubTags }
				</div>)}
				
				{articlesDom}
			</div>
		)
	}
	changeCluster(e,tag) {
		console.log('e ', e)
		console.log('tag ', tag)

	}
}

export default Main

/*
const MyCloud = (props) => {
	let keywords = []
	let counter = []
	let wiki = []
	let k = []
	if ( props.data ) {
		console.log('my world cloud')
		keywords = props.data.keywords
		counter = props.data.counts
		console.log('props data: ' + props.data)
		k = keywords.map( (i,index) => {
			let fontSize = 5 + (30 / ( keywords.length / 6 + 1 )) + ( 2 / (keywords.length / 10 + 1 ))*counter[index]
			fontSize = fontSize > 75 ? 75 : fontSize
			return (
				<a href={"https://en.wikipedia.org/wiki/"+i}
					style={{fontSize: fontSize}}
					>
					<div>
							{i}
					</div>
				</a>
			)
		})
	}

    return (
      <TagCloud 
        style={{
          fontFamily: 'sans-serif',
          fontSize: 20,
          fontWeight: 'bold',
          color: () => randomColor(),
          padding: 5,
          width: '100%',
          margin: '0 auto',
          maxWidth: '800px',
          height: '500px',
          textAlign: 'left'
        }}>
        {k}
      </TagCloud>
    )
}
*/
const Article = (props) => {
	return (
		<div className="article-container">
			<h2>{props.data.title}</h2>
			<h3>Abstract</h3>
			<section className="abstract">
				
			</section>
		</div>
	)
}