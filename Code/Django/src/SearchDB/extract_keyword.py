# -*- coding: utf-8 -*-
import rake
import os
import re
import sys
import codecs
# =============================================================== #
# Function: extract_keywords									  #
# This function is extracts keywords from the search query  	  #
# typed in the Nordling search bar                                #
#														 		  #
# Inputs:												 		  #
#	uq: Search query according to the user input.   			  #
#	path: File directory of the articles.                         #
#                                        	 			 		  #
# Outputs:												 		  #
#	keywords: Matrix that contains the keywords for each search.  #
# =============================================================== #


def extract_keywords(uq, path):
    i = 0
    # Declare empty arrays
    suggest = []
    keywords = []
    temp = []
    x = []
    # Read the Smart Stoplist or any preferred Stoplist
    rake_object = rake.Rake("Corpus/SmartStoplist.txt")
    # Run the RAKE algorithm
    suggest = rake_object.run(uq)
    for i in range(0, len(suggest)):
                # Filter the candidate keywords based on the word score
        if suggest[i][1] >= 4.0 and suggest[i][1] <= 6:
            x.append(suggest[i][0])

    # Remove entries with numbers
    for i in range(0, len(x)):
        if any(char.isdigit() for char in x[i]) is False:
            temp.append(x[i])
    # Remove the repeating keywords
    for i in range(0, len(temp)):
        if i == 0:
            keywords.append(temp[i])
        else:
            check = 0
            for k in range(0, len(keywords)):
             # If we find a repeating keyword our counter will increase by 1
                if temp[i] in keywords[k] or keywords[k] in temp[i]:
                    check = check+1
            # We only append such word if the counter is 0
            # i.e. we havent seen it on the previous entries
            if check == 0:
                keywords.append(temp[i])
    return keywords


# =============================================================== #
# Function: extract_keywords_from_article                         #
# This function is performs a full text keyword extraction based  #
# word delimiters, phrase delimiters and word freqency.           #

# Inputs:												 		  #
# path: File directory of the articles.                         #
#                                        	 			 		  #
# Outputs:												 		  #
#   s: Matrix that records the keywords for each article.		  #
# =============================================================== #


def extract_keywords_from_article(path):
    # First input is selecting the Stoplist to be used (Smart or Fox)
    # Second input is the the number of characters for each word (Default: 5 characters)
    # Third input is the number of words for each phrase (Default: At most 3 words)
    # Fourth input is the keyword freqeuency (Default: Atleast appear 3 times in the text)
    rake_object = rake.Rake("Corpus/SmartStoplist.txt", 5, 3, 3)
    # Select the correct folder to read articles
    # Declare empty arrays
    files = path
    print(files)
    s = []
    i = 0
    x = []

    if not os.path.isdir(files):
        # Open the files in iteration
        f = codecs.open(files, encoding='utf-8')
        iter_f = iter(f)
        str = ""
        keywordarr = []
        # Read the text files
        for line in iter_f:
            str = str + line
        # Run the RAKE algorithm
        keywordarr = rake_object.run(str)
        for j in range(0, len(keywordarr)):
            # Filter candidate keywords with a threshold word score
            if keywordarr[j][1] >= 2.0:
                s.append(keywordarr[j][0])

    return s
