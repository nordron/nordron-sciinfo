import React from 'react'
import './main.sass'
import TagCloud from 'react-tag-cloud'
import randomColor from 'randomcolor'


class Main extends React.Component{
	constructor(props) {
		super(props)
		this.state = {
			data: this.props.defaultData
		}
	}
	render() {
		return (
			<div id="main-page">
				<img 
					src="http://www.londoncleaningcarpets.co.uk//images/layout/close-btn.png" 
					className="close-icon"
					onClick={ ()=>{
						let DOM = document.getElementById('article-detail')
						DOM.style.display = 'none'
					}}
					/>
				<MyCloud data={this.state.data}/>
				<Article data={this.state.data}/>
			</div>
		)
	}
}

export default Main


const MyCloud = (props) => {
	let keywords = []
	let counter = []
	let wiki = []
	let k = []
	if ( props.data ) {
		console.log('my world cloud')
		keywords = props.data.keywords
		counter = props.data.counts
		console.log('props data: ' + props.data)
		k = keywords.map( (i,index) => {
			let fontSize = 5 + (30 / ( keywords.length / 6 + 1 )) + ( 2 / (keywords.length / 10 + 1 ))*counter[index]
			fontSize = fontSize > 75 ? 75 : fontSize
			return (
				<a href={"https://en.wikipedia.org/wiki/"+i}
					style={{fontSize: fontSize}}
					>
					<div>
							{i}
					</div>
				</a>
			)
		})
	}

    return (
      <TagCloud 
        style={{
          fontFamily: 'sans-serif',
          fontSize: 20,
          fontWeight: 'bold',
          color: () => randomColor(),
          padding: 5,
          width: '100%',
          margin: '0 auto',
          maxWidth: '800px',
          height: '500px',
          textAlign: 'left'
        }}>
        {k}
      </TagCloud>
    )
}

const Article = (props) => {
	return (
		<div className="article-container">
			<h2>{props.data.title}</h2>
			<h3>Abstract</h3>
			<section className="abstract">
				
			</section>
		</div>
	)
}