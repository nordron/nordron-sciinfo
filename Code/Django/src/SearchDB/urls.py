# -*- coding: utf-8 -*-
"""============================================================
 Purpose:  match URL to views.py
 Reference: https://tutorial.djangogirls.org/en/django_urls/
 Relative file: views.py
============================================================"""

from django.conf.urls import url
from . import views
from django.views.generic import RedirectView


app_name = 'SearchDB'
urlpatterns = [
    url(r'^$', views.get_text, name='get_text'),
    url(r'^update/$', views.refreshDatabase, name='refreshDatabase'),
    url(r'^Upload/', views.list, name='list'),
]
