# -*- coding: utf-8 -*-
"""====================================================================
 Purpose: provide decorator for registering classes
 Reference: https://docs.djangoproject.com/en/2.0/ref/contrib/admin/
===================================================================="""

from __future__ import unicode_literals
from django.contrib import admin
from .models import Article, Document

# Register your models here
admin.site.register(Article)
admin.site.register(Document)
