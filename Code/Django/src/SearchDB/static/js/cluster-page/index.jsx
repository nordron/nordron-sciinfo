import React from 'react'
import ReactDOM from 'react-dom'
import Main from './main.jsx'

const clusterButton = document.querySelector("#cluster-function")

clusterButton.onclick = () => {
	let DOM = document.querySelector('#cluster-page')
	/*
	let data = {
		words: ['deep learning', 'algorithm', 'AI', 'neural network', 'deep'],
		counts: [10,2,3,4,5],
		title: 'Open Information Systems Semantics for Distributed Artificial Intelligence',
		abstract: 'Hewitt, C., Open Information Systems Semantics for Distributed Artificial Intelligence,Artificial Intelligence 47 (1991) 79-106.Distributed Artificial Intelligence (henceforth called DAI) deals with issues of large-scaleOpen Systems (i.e. systems which are always subject to unanticipated outcomes in their operation and which can receive new information from outside themselves at any time). Open Information Systems (henceforth called OIS) are Open Systems that are implemented using digital storage, operations, and communications technology. OIS Semantics aims to provide a scientific foundation for understanding such large-scale OIS projects and for developing new technology. The literature of DAI speaks of many important concepts such as commitment, conflict'
	}
	*/
	DOM.style.display = "flex"
	ReactDOM.render(<Main clusterTags={cluster_tag} articles={articles}/>, document.getElementById('cluster-page'))
	//ReactDOM.render(<InputBox  defaultQuery="" />, document.querySelectorAll(".keyword-paragraph"))
}	