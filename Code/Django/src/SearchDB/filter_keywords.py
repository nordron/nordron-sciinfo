# -*- coding: utf-8 -*-
import random

# =============================================================== #
# Function: filter_keywords                                     #
# This function finds the overlapping keywords from the user's  #
# query and the keywords from the articles full text            #

# Inputs:												 		#
# keywords_uq: keywords from the user's query                   #
# keywords_art: keywords from the articles full text            #
#                                        	 			 		#
# Outputs:												 		#
#   keywords_disp: Keywords to be displayed             		#
# =============================================================== #


def filter_keywords(keywords_uq, keywords_art):
    keywords_disp = [[] for _ in range(len(keywords_art))]
    # select the overlapping keywords from the keywords_uq and keywords_art
    for i in range(0, len(keywords_uq)):
        for j in range(0, len(keywords_art)):
            for k in range(0, len(keywords_art[j])):
                # or keywords_art[j][k] in keywords_uq[i]:
                if keywords_uq[i] in keywords_art[j][k]:
                    if len(keywords_disp[j]) < 5.0:
                        keywords_disp[j].append(keywords_art[j][k])

    # Add random keywords until the array is length of 5
    for i in range(0, 5):
        for j in range(0, len(keywords_art)):
            if len(keywords_disp[j]) < 5.0:
                # if len(keywords_art[j])>2:
                # print('#################')
                # print(keywords_art[j])
                r = random.randint(0, len(keywords_art[j])-1)
                check = 0
                # else:
                # r = 0
                # eliminate the possibility of repeated leywords
                for k in range(0, len(keywords_disp[j])):
                    if keywords_art[j][r] in keywords_disp[j][k]:
                        check = check+1
                if check == 0:
                    keywords_disp[j].append(keywords_art[j][r])
    return keywords_disp
