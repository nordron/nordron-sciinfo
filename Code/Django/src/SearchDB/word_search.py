import os
import numpy as np
import wikipedia
from os import listdir
from nltk.tokenize import sent_tokenize
from os import listdir
import codecs

# =============================================================== #
# Function: word_search											#
# This function is using wikipedia API in order to return the 	#
# definition of certain keywords.           		 			#

# Inputs:												 		#
# keywords: Keywords according to the user inputs. 		        #
#             Should be corresponding to each article.			#

# file_names: array of names of articles.			 		    #
# Outputs:												 		#
# POT: Matrix that contains sentences of keywords.			    #
# counter: Matrix that record the times each keyword appears.	#
# =============================================================== #


def word_search(keywords, file_names):
    # Declare the size of POT: Fixed sized of keywords is 5
    POT = [[[] for _ in range(5)] for _ in range(len(file_names))]
    counter = []  # To record how many time each keyword is found in each article

    for ii in range(0, len(file_names)):  # Read each article in the folder
        with codecs.open(file_names[ii], encoding='utf-8') as openfile:
            text = []
            for line in openfile:
                text.append(line)
        sentences = []
        x = []
        for i in range(0, len(text)):
            # Break down the paragraph into sentences
            x.append(sent_tokenize(text[i]))
            for j in range(0, len(x[i])):
                # Store each sentence into lise 'sentences'
                sentences.append(x[i][j])

        # To record the count number of each keyword in an article
        counter_article = np.zeros(len(keywords[ii]))

        for jj in range(0, len(keywords[ii])): 	# Search for each keyword
            for kk in range(0, len(sentences)):	 # in each sentence
                # Check if the jj-th keyword in kk-th sentence
                if keywords[ii][jj] in sentences[kk]:
                    if len(POT[ii][jj]) < 1:
                        if kk == 0:  # The keyword appears in the first sentence of the paragraph
                            POT[ii][jj].append(
                                sentences[kk]+' '+sentences[kk+1])

                        # The keyword appears in the last sentence of the paragraph
                        if kk == len(sentences):
                            if sentences[kk] not in POT[ii][jj]:
                                POT[ii][jj].append(
                                    sentences[kk-1]+' '+sentences[kk])

                        else:  # The keyword appears in the middle of the paragraph
                            if sentences[kk] not in POT[ii][jj]:
                                POT[ii][jj].append(
                                    sentences[kk-1]+' '+sentences[kk]+' '+sentences[kk+1])

                        counter_article[jj] = counter_article[jj] + 1
                    if len(POT[ii][jj]) < 1:
                        POT[ii][jj].append('not found')
        # Append the count number of each keyword of each article to list 'count'
        counter.append(list(counter_article))

    return POT, counter

# =============================================================== #
# Function: get_wiki_summary									#
# This function is using wikipedia API in order to return the 	#
# definition of certain keywords.           		 			#

# Inputs:												 		#
#	keywords: the words that user wants to look for definition. #

# Outputs:												 		#
#	A matrix that contains the definition of the input words.	#
# =============================================================== #


def get_wiki_summary(keywords):
    # Declare a matrix for wikipedia results, which is the same size as keywords matrix
    wiki_summary = [[] for emp_list in range(len(keywords))]
    for ii in range(len(keywords)):
        for jj in range(len(keywords[ii])):
            # Get the summary of the keyword from wikipedia api
            wiki_summary[ii].append(wikipedia.summary(keywords[ii][jj]))
    return wiki_summary


#========================================================#
# Function: get_keyword_counts							 #
# This function is used to format the dictionary, which  #
# is need in the front end to display wordcloud.		 #
#														 #
# Inputs:												 #
#	keywords, counter are from function "word_search.py" #
#	article_path: path to the txt files					 #
#														 #
# Outputs:												 #
#	A dictionary contains the title, keywords and the 	 #
#	counted time of each keyword of each article.		 #
# 														 #
# output = [{'title':..., 'keywords': [...], 		     #
#			 'counts':..., 'summaries':... 				 #
#            }, ...]									 #
#========================================================#
def get_keyword_counts(keywords, counter, file_names):
    # change this line to do it for specific filenames

    # Declare empty array
    frequency = []
    #wiki_summary = get_wiki_summary(keywords)

    for ii in range(len(file_names)):
        dic = {}
        dic['title'] = file_names[ii]
        dic['keywords'] = keywords[ii]
        dic['counts'] = counter[ii]
        dic['summaries'] = ""  # wiki_summary[ii]
        frequency.append(dic)

    return frequency


def get_counter(keywords, file_names):
    # Declare the size of POT: Fixed sized of keywords is 5
    #POT = [[[] for _ in range(5)] for _ in range(len(file_names))]
    counter = []  # To record how many time each keyword is found in each article

    for ii in range(0, len(file_names)):  # Read each article in the folder
        with codecs.open(file_names[ii], encoding='utf-8') as openfile:
            text = []
            for line in openfile:
                text.append(line)
        sentences = []
        x = []
        for i in range(0, len(text)):
            # Break down the paragraph into sentences
            x.append(sent_tokenize(text[i]))
            for j in range(0, len(x[i])):
                # Store each sentence into lise 'sentences'
                sentences.append(x[i][j])

        # To record the count number of each keyword in an article
        counter_article = np.zeros(len(keywords[ii]))

        for jj in range(0, len(keywords[ii])): 	# Search for each keyword
            for kk in range(0, len(sentences)):	 # in each sentence
                # Check if the jj-th keyword in kk-th sentence
                if keywords[ii][jj] in sentences[kk]:
                    # if len(POT[ii][jj]) < 1:
                    counter_article[jj] = counter_article[jj] + 1
                    '''if len(POT[ii][jj]) < 1:
                        POT[ii][jj].append('not found')'''
        # Append the count number of each keyword of each article to list 'count'
        counter.append(list(counter_article))

    return counter
