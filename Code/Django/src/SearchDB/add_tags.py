import operator
# ==================================================================#
# The function is splitted into 2 layers. In each layer, we will   #
# calculate the top ranked keywords based on its frequency and add #
# them to the articles, which contain the keyword, as tags.        #
# Those tags will work as indicators for clustering in front end.  #
# ==================================================================#

# ==================================================================#
# Functions for the first layer								   	   #
# ==================================================================#


def get_all_counts(keyword_counts):
    all_counts = {}
    for ii in range(len(keyword_counts)):
        for jj in range(len(keyword_counts[ii]['keywords'])):
            if keyword_counts[ii]['keywords'][jj] in all_counts:
                all_counts[keyword_counts[ii]['keywords']
                           [jj]] += keyword_counts[ii]['counts'][jj]
            else:
                all_counts[keyword_counts[ii]['keywords']
                           [jj]] = keyword_counts[ii]['counts'][jj]

    sorted_all_counts = sorted(all_counts.items(), key=operator.itemgetter(1))

    return sorted_all_counts

# Get top 3 keywords for 1 layer tag


def get_tags(all_counts):
    tags = []
    # sorted_all_counts = sorted(all_counts.items(), key=operator.itemgetter(1))
    for ii in range(0, 3):
        tags.append(all_counts[len(all_counts)-1-ii][0])

    return tags

# Add 1st layer tag to cluster_tag


def first_layer_tag(cluster_tag, tags):
    for ii in range(0, 3):
        cluster_tag[ii]['name'] = tags[ii]
    return cluster_tag

# Add tags to keyword_counts


def add_tag(keyword_counts, cluster_tag):
    for word in cluster_tag:
        for jj in range(len(keyword_counts)):
            if word['name'] in keyword_counts[jj]['keywords']:
                if word['name'] not in keyword_counts[jj]['tags']:
                    keyword_counts[jj]['tags'].append(word['name'])
    return keyword_counts

# ==================================================================#
# Functions for the second layer								   #
# ==================================================================#
# Get 2nd layer keyword_counts


def get_second_layer(keyword_counts_1, cluster_tag):
    keyword_counts_2 = []
    for ii in range(len(cluster_tag)):
        each_keyword = []
        for jj in range(len(keyword_counts_1)):
            if cluster_tag[ii]['name'] in keyword_counts_1[jj]['tags']:
                each_keyword.append(keyword_counts_1[jj])
        keyword_counts_2.append(each_keyword)
    return keyword_counts_2

# Get all counts for each keyword subset


def get_all_count_2(keyword_counts_2, cluster_tag):
    all_counts_2 = []
    for ii in range(len(keyword_counts_2)):
        all_counts_2.append(get_all_counts(keyword_counts_2[ii]))
    # return all_counts_2

# Get tags
# def get_all_tags(cluster_tag, all_counts_2):
    all_tags = []
    for ii in range(len(cluster_tag)):
        each_word = []
        for jj in range(len(all_counts_2[ii])):
            if all_counts_2[ii][len(all_counts_2[ii][jj])-1-jj][0] is not cluster_tag[ii]['name']:
                each_word.append(
                    all_counts_2[ii][len(all_counts_2[ii][jj])-1-jj][0])
        all_tags.append(each_word)
    return all_counts_2, all_tags

# Add to cluster_tag


def second_layer_tag(cluster_tag, all_tags):
    for ii in range(len(cluster_tag)):
        cluster_tag[ii]['subtags'] = all_tags[ii]
    return cluster_tag


def final_keyword_counts(cluster_tag, keyword_counts):
    for word in cluster_tag:
        for ii in range(len(word['subtags'])):
            for jj in range(len(keyword_counts)):
                if word['subtags'][ii] in keyword_counts[jj]['keywords']:
                    if word['subtags'][ii] not in keyword_counts[jj]['tags']:
                        keyword_counts[jj]['tags'].append(word['subtags'][ii])
    return keyword_counts


# ==================================================================#
# Main Function 												   #
# ==================================================================#
def add_tags(keyword_counts):
    cluster_tag = [{
        'name': '',
        'subtags': [],
    },
        {
        'name': '',
        'subtags': [],
    },
        {
        'name': '',
        'subtags': [],
    }]

    # first layer
    all_counts_1 = get_all_counts(keyword_counts)
    tags = get_tags(all_counts_1)
    cluster_tag = first_layer_tag(cluster_tag, tags)
    keyword_counts_1 = add_tag(keyword_counts, cluster_tag)

    # second layer
    keyword_counts_2 = get_second_layer(keyword_counts_1, cluster_tag)
    all_counts_2, all_tags = get_all_count_2(keyword_counts_2, cluster_tag)
    cluster_tag = second_layer_tag(cluster_tag, all_tags)
    keyword_counts = final_keyword_counts(cluster_tag, keyword_counts)

    return cluster_tag, keyword_counts
