



const articlesList = [
	{
		title: 'Gudimella2017_Deep_reinforcement_learning_for_dexterous',
		abstract: 'Deep reinforcement learning yields great results for a large array of problems, butmodels are generally retrained anew for each new problem to be solved. Priorlearning and knowledge are difficult to incorporate when training new models,requiring increasingly longer training as problems become more complex. This isespecially problematic for problems with sparse rewards.',
		keywords: [
			{
				keyword: 'reinforcement learning',
				paragraph: [
					'Learning goal-directed skills is a major challenge in reinforcement learning when the environment’sfeedback is sparse. The difficulty arises from insufficient exploration of the state space by an agent,and results in the agent not learning a robust policy or value function.',
				]
			},
			{
				keyword: 'neural network',
				paragraph: [
					'the problem of exploration in the sparse reward regime is still a significant challenge.Rarely occurring sparse reward signals are difficult for neural networks to model, since the actionsequences leading to high reward must be discovered in a much larger pool of low-reward sequences.'
				]
			}
		] 
	},
	{
		title: 'Efficient BackProp',
		abstract: 'Backpropagation is a very popular neural network learning algorithm becauseit is conceptually simple',
		keywords: [
			{
				keyword: 'back-propagation',
				paragraph: [
					'The convergence of back-propagation learning is analyzedso as to explain common phenomenon observedb y practitioners. Manyundesirable behaviors of backprop can be avoided with tricks that arerarely exposedin serious technical publications'
				]
			}
		] 
	}
]


export default articlesList

