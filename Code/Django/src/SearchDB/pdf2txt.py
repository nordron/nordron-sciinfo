# !/usr/bin/python
# -*- coding: UTF-8 -*-
import io
import os
from pdfrw import PdfReader, PdfWriter
import string
import re
import shutil
import pdfx
import subprocess
from calculators.fleschkincaid import *
from calculators.flesch import *

HOST = os.uname()[1]
DATA_DIR = "/home/" + HOST + "/nordron-sciinfo/Code/Django/src/Articles_pdf/"
TXT_DIR = "/home/" + HOST + "/nordron-sciinfo/Code/Django/src/Articles_txt/"
TEMP_DIR = "/home/" + HOST + "/nordron-sciinfo/Code/Django/src/Articles_pdf_temp/"
# DATA_DIR = "/home/sciinfo/django_image/src/Articles_pdf/"
# TXT_DIR =  "/home/sciinfo/django_image/src/Articles_txt/"

pdf_name = []
txt_name = []


def pdf2txt():
    '''
   This function is used
    to convert pdf to txt in DATA_DIR, so make sure the paths are proper.
   If the txt file is already there, then it would be redundant
    '''
    if not os.path.exists(TXT_DIR):
        os.mkdir(TXT_DIR)

    for filename in os.listdir(DATA_DIR):
        txt_name.append(TXT_DIR + filename.replace(".pdf", ".txt"))
        if not filename.replace(".pdf", ".txt") in os.listdir(TXT_DIR):
            print(filename)
            if (filename[-4:len(filename)].lower() == ".pdf"):
                pdf_name.append(filename)
                pdf = pdfx.PDFx(DATA_DIR + filename)
                text = pdf.get_text()
                new_string = text.encode(
                    'ascii', 'replace').decode('utf8', 'replace')
                xml = io.open(TXT_DIR + filename.replace(".pdf",
                                                         ".txt"), 'w', encoding='utf8')
                xml.write(new_string)
                xml.close()
    return txt_name


def pdf2txt_(DATA_DIR_TEMP, TXT_DIR, newfiles):
    '''
   This function is used to convert pdf to txt in DATA_DIR_TEMP, so make sure the paths are proper.
   If the txt file is already there, then it would be redundant
    '''
    # if not os.path.exists(TXT_DIR):
    #    os.mkdir(TXT_DIR)

    for filename in newfiles:
        txt_name.append(TXT_DIR + filename.replace(".pdf", ".txt"))
        if not filename.replace(".pdf", ".txt") in os.listdir(TXT_DIR):
            print(filename)
            if (filename[-4:len(filename)].lower() == ".pdf"):
                pdf_name.append(filename)
                pdf = pdfx.PDFx(DATA_DIR_TEMP + filename)
                text = pdf.get_text()
                xml = io.open(TXT_DIR + filename.replace(".pdf",
                                                         ".txt"), 'w', encoding='utf8')
                xml.write(text)
                xml.close()
    return txt_name


def conv2utf8(files, TXT_DIR):
    '''
   This funtion This function will remove the U+25A1 character from text files
   Input : files / [] (do to all files in TXT_DIR)
    '''
    if not files:
        for filename in os.listdir(TXT_DIR):
            subprocess.call(
                ['sed', '-i', r"s/[^[:print:]\t]//g", TXT_DIR + filename])

    else:
        for filename in files:
            subprocess.call(['sed', '-i', r"s/[^[:print:]\t]//g", filename])


def countWordNumber(files, TXT_DIR):
    '''
   This funtion is used to count words of the articles
   Input :  (full path  + file name )files / [] (do to all files in TXT_DIR)
    '''
    num_words = 0
    if files == []:
        for filename in os.listdir(TXT_DIR):
            with io.open((TXT_DIR + filename), 'r+') as f:
                for line in f:
                    words = line.split()
                    num_words += len(words)
                if line[0:11] == '# of words:':
                    break
                f.write(u'\n# of words: ' + str(num_words).decode("utf-8"))
                f.close()

    else:
        for filename in files:
            with io.open(filename, 'r+') as f:
                for line in f:
                    words = line.split()
                    num_words += len(words)
                if line[0:11] == '# of words:':
                    break
                f.write(u'\n# of words: ' + str(num_words).decode("utf-8"))
                f.close()

    return num_words


def reading_score(files, TXT_DIR):
    '''
   This funtion is used to calculate the reading score/ease for each file
   Input : files / [] (do to all files in TXT_DIR)
    '''
    if not files:
        for filename in os.listdir(TXT_DIR):
            fl = Flesch(
                open(TXT_DIR + filename).read())

            fk = FleschKincaid(
                open(TXT_DIR + filename).read())

            with open(TXT_DIR + filename, 'a') as f:
                f.write("Flesch reading ease score: %.1fFlesch reading grade is (US): %.1f" % (
                    fl.reading_ease, fk.us_grade))
                f.close

    else:
        for filename in files:
            fl = Flesch(
                open(filename).read())

            fk = FleschKincaid(
                open(filename).read())

            with open(filename, 'a') as f:
                f.write("Flesch reading ease score: %.1fFlesch reading grade is (US): %.1f" % (
                    fl.reading_ease, fk.us_grade))
                f.close
    return [fl.reading_ease, fk.us_grade]


def deleteFinalLine(files):
    '''
   This funtion is used to delete the final line of articles
   Input : files / [] (do to all files in TXT_DIR)
    '''
    if files == []:
        for filename in os.listdir(TXT_DIR):
            os.system('sed -i "$ d" {0}'.format(TXT_DIR + filename))

    else:
        for filename in files:
            os.system('sed -i "$ d" {0}'.format(filename))


def renameFileToPDFTitle_(fullName):
    '''
   This funtion is used to rename the file. title, date,
   author are exttact from metadata. If any information is missing ...

   Input : path + files
   output : new file names
    '''
    path, file = os.path.split(fullName)
    # Extract pdf title from pdf file
    reader = PdfReader(fullName)
    newName = reader.Info.Title
    newDate = reader.Info.ModDate
    newAuthor = reader.Info.Author
    printable = set(string.printable)
    if newName != None:
        newName = filter(lambda x: x in printable, newName)
        newName = newName.strip('()')
        newName = newName.replace(" ", "-")
    else:
        newName = ''
    if newAuthor != None:
        newAuthor = filter(lambda x: x in printable, newAuthor)
        newAuthor = newAuthor.strip('()')
        newAuthor = newAuthor.replace(" ", "_")
    else:
        newAuthor = ''

    if newDate != None:
        newDate = filter(lambda x: x in printable, newDate)
        datepat = re.compile(r'\d+')
        year = datepat.findall(newDate)[0]
        year = year[0:4]
    else:
        year = ''
    # print PdfReader(fullName).Info
    # Remove surrounding brackets that some pdf titles have
    if newName == '' or newAuthor == '' or year == '':
        print('no tilte')

    else:
        newName = year + '_' + newAuthor + "_" + newName + '.pdf'
        os.rename(fullName, path + "/" + newName)
        return newName


def renameFileToPDFTitle(path):
    '''
    call  renameFileToPDFTitle_
    This funtion is used to rename the file in the given path.
    Input : path
    Output : new file name
    '''
    newFile = []
    for fileName in os.listdir(path):
        # Rename only pdf files
        fullName = os.path.join(path, fileName)
        if (not os.path.isfile(fullName) or fileName[-4:] != '.pdf'):
            continue
        r = renameFileToPDFTitle_(path + fileName)
        if r != None:
            newFile.append(r)

    return newFile


def moveFile(filenames, src, dst):
    '''
    This fuction is used to move the file
    Input : files, source path, destination path
    '''
    for filename in filenames:
        shutil.move(src + filename, dst + filename)
        print filename + 'is moved to ' + dst


def changeFileMetadata_(filename, newTitle, newDate, newAuthor):
    '''
    Input : path+ filename, newTitle, newDate, newAuthor
    '''
    inpfn = filename
    outfn = 'alter.' + os.path.basename(inpfn)

    trailer = PdfReader(inpfn)
    trailer.Info.Title = newTitle
    trailer.Info.Date = newDate
    trailer.Info.Author = newAuthor
    PdfWriter(outfn, trailer=trailer).write()


def getFileMetadata_(fullName):
    '''
    This function is used to get the infomation of articles, which
    extracts metadata like "rename function" did.
    Input : path + filename
    Output : newName, newAuthor, year
    '''
    reader = PdfReader(fullName)
    newName = reader.Info.Title
    newDate = reader.Info.ModDate
    newAuthor = reader.Info.Author
    printable = set(string.printable)
    try:
        newName = filter(lambda x: x in printable, newName)
    except:
        newName = 'NoName'
    try:
        newAuthor = filter(lambda x: x in printable, newAuthor)
    except:
        newAuthor = 'NoAuthor'

    newName = newName.strip('()')
    newAuthor = newAuthor.strip('()')
    if newDate != None:
        newDate = filter(lambda x: x in printable, newDate)
        datepat = re.compile(r'\d+')
        year = datepat.findall(newDate)[0]
        year = year[0:4]
    else:
        year = ''

    return [newName, newAuthor, year]


def readFinalLine(files, TXT_DIR):
    '''
        This funtion is used to read the final line of the txt files where the information
        is written.
        Input :  (full path  + file name )files / [] (do to all files in TXT_DIR)
                txt foler path
        Ouput : the last line
    '''
    if files == []:
        for filename in os.listdir(TXT_DIR):
            with open(TXT_DIR+filename, "rb") as f:
                # first = f.readline()        # Read the first line.
                f.seek(-2, os.SEEK_END)     # Jump to the second last byte.
                while f.read(1) != b"\n":   # Until EOL is found...
                    # ...jump back the read byte plus one more.
                    f.seek(-2, os.SEEK_CUR)
                last = f.readline()         # Read last line.
    else:
        for filename in files:
            with open(filename, "rb") as f:
                # first = f.readline()        # Read the first line.
                f.seek(-2, os.SEEK_END)     # Jump to the second last byte.
                while f.read(1) != b"\n":   # Until EOL is found...
                    # ...jump back the read byte plus one more.
                    f.seek(-2, os.SEEK_CUR)
                last = f.readline()         # Read last line.
    return last


def getFinalLinesInfo(string):
    '''
        This funtion is used to extract the information including [(#words),(Flesch reading ease score),(( Flesch reading grade is (US))from final line.
        Input :  string
        Output : list [#words,Flesch reading ease score, Flesch reading grade is (US) ]
    '''
    m = string.find('# of words:')
    n = string.find('Flesch reading ease score:')
    l = string.find('Flesch reading grade is (US):')
    # print  string[m+12:n], string[n+27:l], string[l+31:]
    return [string[m+12:n], string[n+27:l], string[l+31:]]

# The following is for finding a line with a certain text and givng back the
# information. That is useful because we can store information such as statistics
# in a txt file and anyone can then later on read that data and analyse it.


def get_value_from_line(files, str):
    """
        Get number-value after a substring.

        Args:
                files (str): File in which you are looking for a substring (e.g. number of words).
                substr (str): Substring to look for in lines.

        Returns:
                [int]: Numbers in that line.
    """
    lines = []
    for line in files.strip().split('\n'):
        if substr in line:
            re.findall(r'\d+', str)
    return number


if __name__ == "__main__":
    # --------Test - transfer to txt + count the word ---
    # txts = pdf2txt()
    # wordNum = countWordNumber([], TXT_DIR)
    # readingScore = reading_score([], TXT_DIR)
    # deleteFinalLine(txts)

    # --------Test - process upload file------------
    # newFiles = renameFileToPDFTitle(TEMP_DIR)
    # moveFile(newFiles, TEMP_DIR, DATA_DIR)
    # newTxt = pdf2txt_(DATA_DIR, newFiles)
    # wordNum = countWordNumber(newTxt)
    # --------Test - get info wirtten in final line-------------
    last = readFinalLine([], TXT_DIR)
    finalLnInfo = getFinalLinesInfo(last)
    print finalLnInfo
