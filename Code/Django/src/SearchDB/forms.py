from django import forms


class TextForm(forms.Form):
    your_text = forms.CharField(label='Your text')

# Basic form for upload


class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=50)
    file = forms.FileField()


class DocumentForm(forms.Form):
    docfile = forms.FileField(
        label='Select a file',
        widget=forms.ClearableFileInput(attrs={'multiple': True}
                                        ))
