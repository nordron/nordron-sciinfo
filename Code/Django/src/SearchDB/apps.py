# -*- coding: utf-8 -*-
"""=====================================================================
 Purpose:  include any application configuration for the app
 Reference: https://docs.djangoproject.com/en/2.0/ref/applications/
====================================================================="""

from __future__ import unicode_literals
from django.apps import AppConfig


class SearchdbConfig(AppConfig):
    name = 'SearchDB'
