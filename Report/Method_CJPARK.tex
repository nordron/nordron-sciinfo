%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Team: CJ PARK
% Members: Cindy Hsu, Jose Chang, Paul Tsai, Alan Yu, Ralph Nasara, Kevin Liao
% Relative files:
% Main.tex 
% Note: Do not compile this file compile Main.tex to get the pdf file instead.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Spellcheck function}

Writing correct keywords allows users to rapidly and efficiently find the best articles related to query. 
Misspelling of words in the search string leads to a futile search and thus a suboptimal experience for the researcher who wants to find articles as efficiently as possible.
For this reason, we have developed a spellchecking system that automatically checks for the correct spelling of words on-change in the search text area.

Once a change event is fired, the script splits the query into individual letters and spellcheck is performed once the user has keyed-in a `` " (blank space). 
If the word fails the spellcheck test, then the algorithm will return a list of suggestions for the correct spelling.
The script repeats the spellcheck test for every word in the query, and store the words in an array.

The spellcheck function was implemented in the front end of the webpage and was written in Javascript using the \href{https://github.com/alexcorvi/spelt}{Spelt} package.  

Spelt is a JavaScript English spellchecker, under MIT license, written in TypeScript.
Spelt is a package that bases its spellchecking method on the Levenshtein distance (LD) while at the same time considering the repositioning of the letter in a word.

\section{Obtaining the synonyms}

Keywords, a sequence of more than one word, provides a compact representation of a document's content. 
However, contemporary publications may fail to truthfully represent the essential content of documents by letting users assign their keywords or select from an already pre-defined list. 
With the depth of digital information available, we no longer know what to find and how to find them easily. 
Keywords are imperative for effective information retrieval.

To increase the quality of user search results, users need to understand the process of Query Expansion (QE).
A facet of QE is finding the synonym of the keywords and also search for those synonyms to expand the search query to match additional documents.
However, most users do not always know how to formulate search queries, and they see it as an intimidating task. 
To help users, we have implemented a synonym generation function by web crawling results from \url{www.thesaurus.com}. Web crawling is not illegal but is still subject to scrutiny to the \href{http://www.dictionary.com/e/terms/}{Terms of Service of Dictionary.com, LLC}.

To implement this function, we first extract keywords from the user's query using Rapid Automatic Keyword Extraction (RAKE). 
The results are then post-processed by removing any possibilities of repeating keywords, short keywords encased by larger keywords, keywords containing numbers, and strange characters. 
The resulting synonyms (only top two synonyms) are displayed as interactive tags just below the search bar.
Future implementation will trigger a search when users click the interactive tags.
 
\section{Article keyword extraction}

We have implemented a python package called \href{https://github.com/zelandiya/RAKE-tutorial}{Rapid Automatic Keyword Extraction} (RAKE) algorithm as described by \cite{Rose2010} in their work ``Automatic Keyword Extraction from Individual Documents". 
The source code is released under the MIT license. 
RAKE is an unsupervised, domain-independent, and language-independent method for extracting keywords from individual documents.
RAKE is an efficient automatic keyword extraction technology which uses a simple set of input parameters and a stoplist. 
Keyword extraction is single pass with high precision and good recall, making it an ideal tool for a database with a wide range of documents.

Essential to developing RAKE is defining stop words. 
Stop words such as ``and", ``the" and ``of" have no significant lexical meaning and frequently appear in the document. 
In contrary, content words are the essential words which bear content of the document.
To configure RAKE, we provided a stoplist (a list of stop words, or negative dictionary) and a set of phrase and word delimiters.
RAKE parses a full text into an array of words specified by the word delimiters, which is further split into sequences based on the phrase delimiters and stop words. 
Co-occurrences of words within this sequence help to identify the candidate keywords.

The algorithm generates all the candidate keywords and calculates the word score. For multiple words, the word score is the sum of its member scores. 
There are three metrics to evaluate word scores based on word degree and word frequency.
Since RAKE uses standard punctuation and stops words as keyword parsers, therefore, it would technically miss candidate keywords like ``center of mass" which contains an interior stop word. 
The algorithm looks for pair keywords that adjoin one another at least twice in the same document and in the same order to find this kind of keywords.
The new keyword is the sum of its member keyword scores. Therefore, candidate keywords of a higher order (more word phrases and adjoined by stop words) will have higher scores. 
From a list of candidate keywords, the algorithm ranks the scores, and the top $T$ scoring candidates are selected. 
Variable $T$, in this case, is approximated as one-third the number of candidate keywords. 
The results for this algorithm are compared with the keywords extracted from the user's search query, and the overlapping keywords are considered relevant keywords that will be searched on the articles through the word search feature.

\section{Keyword search in articles}

Allowing the user to quickly identify and filter articles that are useful for him or her is a feature we intend to achieve to the keyword search in articles function. 
The Keyword search feature uses the keywords extracted to search for instances on the articles where the keyword is present and return a random instance for display, as well as the frequency of the searched keyword on the text. 
The user should be able to click on the keyword, and the area will expand showing an instance in the article where we find such keyword.

The implementation of this feature consists of tokenizing the lines of each article into individual sentences through the use of \href{https://www.nltk.org/}{Natural Language Toolkit} (NLTK) library which is an open source Python package. 
NLTK is a platform for building Python programs to work with human language data.
Similarly to our information retrieval software, the NLTK source code released under the Apache 2.0 license in 2001.
It provides interfaces such as \href{https://wordnet.princeton.edu/}{WordNet}, along with a suite of text processing libraries for classification, tokenization, stemming, tagging, parsing, and semantic reasoning.

With the text divided into sentences, we searched for the specific keyword and returned the sentences for every instance (previous, current and subsequent sentences) in the text where the keyword appears.
This process is done iteratively for every keyword for every article.
The results are then appended together and sent to the front end for display.

\section{Wordcloud visualization}

A word cloud visualization or also referred to as Tag Cloud is a simple and powerful tool to give users associated keywords that they might have missed during the QE, at a glance.
The information provided is displayed words where the size of each word proportional to its frequency as shown in Figure \ref{fig:wordcloud}. 
This visualization allows the user to quickly identify the article's field through its keywords and the frequency with which there are used in the text and rapidly determine the article's relevancy for him.


\begin{figure}
\begin{centering}
	\includegraphics[width=7cm]{CJPARK_Wordcloud_visualization.png}
	\caption{Anatomy of a Wordcloud}
	\label{fig:wordcloud}
\end{centering}
\end{figure}


For implementing this feature, we merely performed our previous extract keyword function on the full text of the article followed by our word search function. 
This function allowed us to have the keywords of the article and their frequencies. 
With this information, it is just a matter of displaying it to the user through the use of the word cloud.
It is worth noticing that the base size of the keywords in the word could $s$ is determined by the number of keywords $n$ extracted from the article plus a constant factor $\alpha$ multiplied by each keyword's frequency $\omega$.

\begin{align*}
	s= n + \alpha \omega 
\end{align*}  

Lengthy articles with a high word count will yield a higher number of keywords and thus to avoid having the word cloud so extended and huge that we are unable to display to the user we have implemented this correcting function to the keyword's font size. 

\section{Document clustering}
With a rapidly increasing document database and high volumes of data browsing, researchers usually encounter difficulties in searching relevant literature.
Document clustering and its importance will continue to grow as the number of articles in the literature also grow.
Document clustering organizes clusters into similarity based on the returned keywords and their frequency to facilitate more comfortable browsing.

The clusters can be viewed as a topic-subtopic, relationship, and meaningful clusters and labels to facilitate better information retrieval. 
Document clustering is a non-supervised and automatic grouping of texts (synopses or any document) into clusters in which documents within the cluster have high similarity, but are different to documents within another cluster.
Document clustering needs preprocessing steps such as removal of stopwords and stemming of documents, which we already implemented in previous packages.

From the earlier full-text keyword extraction, those keywords are now the strings representing the essence of a document.
We have implemented a python package which uses the already extracted information such as keywords and their frequency. 
A representative cluster tag is determined by the algorithm, along with their separate subtags.
These interactive tags serve as filter and groups the document based on their relevance to the selected clusters.
After each search, the user has an option to click ``Show cluster".


The first layer of tags is on the upper part of the pop-up window display ``All" and up to three other cluster tags.
When selecting, ``All" the documents with titles and abstracts from the initial search query are displayed.
Clicking on another cluster tag opens a set of subtags and the documents matching that cluster.
The subtags display a list of keywords which can is useful for Query Expansion.
The RAKE algorithm can be further optimized to filter representative keywords

