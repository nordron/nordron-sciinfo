# -*- coding: utf-8 -*-
"""=========================================================================================
 Purpose: This will processes an HTTP request, fetches data from the database as needed,
          generates an HTML page by rendering this data using an HTML template,
          and then returns the HTML in an HTTP response
 Reference: https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Home_page
 Basic Relative File: models.py  urls.py  templates/searchBD/*.html
 Other Relative File: abstract.py  similarity_cp1.py  doi_extract_cp1.py pdf2txt.py
                      vector_space_convert_cp1   word_search.py    result_of_year.py
                      fusioncharts.py extract_keyword.py filter_keywords.py word_search.py
==========================================================================================="""

from __future__ import unicode_literals
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector
from django.conf import settings
from django.core.urlresolvers import reverse
from django.template import RequestContext

# Import Article model, Document model(upload used)
from .models import Article, Document
from .forms import UploadFileForm, DocumentForm

# Import modules for vector space convert, similarity, title extraction
import logging
import codecs
import io
import os
import re
import json
import ast
import sys
# 匯入斷字、斷字處理等等手續都在vector_space_convert_cp1、transformation_cp1
# Use "vector_space_convert_cp1" & "transformation_cp1" to tokenize
from collections import defaultdict
from six import iteritems
from pyPdf import PdfFileWriter, PdfFileReader
from vector_space_convert_cp1 import file_read, vector_space_convert
from transformation_cp1 import transformation
from result_of_year import year_similarity_compare
from doi_extract_cp1 import doi_extract

# Import charts
from fusioncharts import FusionCharts
from extract_keyword import extract_keywords_from_article, extract_keywords
from filter_keywords import filter_keywords
from word_search import word_search, get_keyword_counts, get_wiki_summary, get_counter

sys.path.append('../')
from .pdf2txt import pdf2txt_, renameFileToPDFTitle, moveFile, countWordNumber, getFileMetadata_, getFinalLinesInfo, readFinalLine
from .pdf2txt import conv2utf8, reading_score
from .abstract import getAbstract
# Import BeautifulSoup
import requests
from bs4 import BeautifulSoup
from random import randint

# import cluster add tags function
from add_tags import add_tags

# Define path and filename for gensim
PDF_PATH = getattr(settings, 'PDF_PATH', os.path.join(
    settings.BASE_DIR, 'Articles_pdf/'))
TXT_PATH = getattr(settings, 'TXT_PATH', os.path.join(
    settings.BASE_DIR, 'Articles_txt/'))
ABSTRACT_PATH = getattr(settings, 'ABSTRACT_PATH', os.path.join(
    settings.BASE_DIR, 'Articles_abstract/'))
TMP_PATH = getattr(settings, 'TMP_PATH',
                   os.path.join(settings.BASE_DIR, 'tmp/'))
tmpName = 'deerwester'
CORPUS_PATH = getattr(settings, 'CORPUS_PATH', os.path.join(
    settings.BASE_DIR, 'Corpus/'))
PDF_TEMP_PATH = getattr(settings, 'PDF_TEMP_PATH', os.path.join(
    settings.BASE_DIR, 'Articles_pdf_temp/'))


# Create your views here.


def get_text(request):
    # If the search bar gets query (denoted as "q"), redirect to the result page,
    # Using GET method
    if 'q' in request.GET:

        # Access the database to do searching
        article_all = Article.objects.all()
        vector = SearchVector('content', weight='A')
        query = SearchQuery(str(request.GET['q']))
        uq = request.GET['q']

        key_words = extract_keywords(uq, CORPUS_PATH)
        wordsFromSentence = extract_keywords(uq, 'Corpus/')
        synonym = getSynonyms(wordsFromSentence)
        teststr = "Got the message"

        # Implement searching function and ranks
        yearsort = year_similarity_compare(uq, os.listdir(
            TXT_PATH), TMP_PATH, TMP_PATH, TMP_PATH, tmpName)

        year_all = []
        year_simus = []
        # if no article found, then return to search page
        if yearsort == []:
            return render_to_response('SearchDB/search.html')
        # Create list and sorting similarity for each year
        for i in range(len(yearsort)):
            year_all.append(yearsort[i][2])

        year_max = max(year_all)
        year_min = min(year_all)

        for i in range(year_max, year_min - 1, -1):
            tmp = []
            for j in range(0, len(yearsort)):
                if i == yearsort[j][2]:
                    tmp.append(
                        [yearsort[j][0], yearsort[j][1], yearsort[j][2]])
            tmp = sorted(tmp, key=lambda item: -item[1])
            for k in range(0, len(tmp)):
                result = Article.objects.get(filename=tmp[k][0])
                year_simus.append([str(result.filename), tmp[k][1], tmp[k][2]])

        resultlist = []
        abstract = []
        filenames = []
        for i in range(0, len(year_simus)):
            result = Article.objects.get(filename=year_simus[i][0])
            # to obatain an array with the file_names
            filenames.append(TXT_PATH + result.filename)
            file_open = codecs.open(
                ABSTRACT_PATH + result.filename.replace(".txt", "abstract.txt"), 'r', encoding='utf-8')
            read_file = file_open.read()
            abstract.append([(read_file)])

            file_open.close()

            # Read the final line (# of words)(Flesch reading ease score)(Flesch reading grade) of txt file
            last = readFinalLine(filenames, TXT_PATH)
            finalLnInfo = getFinalLinesInfo(last)

            resultlist.append([str(result.filename.split(".")[0][5:].partition(
                "_")[2].partition("_")[0].replace("-", " ").replace("_", " ")), year_simus[i][1], year_simus[i][2],
                str(result.filename[5:].partition("_")[0]
                    ), result.filename.replace(".txt", ".pdf"), finalLnInfo[0], finalLnInfo[1], finalLnInfo[2]])
        total = len(resultlist)
        fig = chart(yearsort)
        data = linechart(year_simus)
        xdata = data[0]
        ydata = data[1]

        # Create object with keywords from Articles and their POT's

        articleData = []
        keywords = [extract_keywords_from_article(f) for f in filenames]
        keywords_uq = extract_keywords(uq, 'Corpus/')
        relevant_keywords = filter_keywords(keywords_uq, keywords)
        POT, counter = word_search(relevant_keywords, filenames)
        counter2 = get_counter(keywords, filenames)

        wordcloud_data = get_keyword_counts(keywords, counter2, filenames)
        print("*******************")
        print(wordcloud_data)

        # add tag and cluster for articles #

        for i in wordcloud_data:
            i["tags"] = []
        cluster_tag, article_with_tag = add_tags(wordcloud_data)

        ##########################################################
        files = filenames
        for i in range(0, len(files)):
            articleObj = {
                "title": files[i],
                "keywords": []
            }
            # get the keywords from article
            for j in range(0, len(relevant_keywords[i])):
                keywordObj = {
                    "keyword": '', "paragraph": POT[i][j], 'keyword': relevant_keywords[i][j]}
                articleObj['keywords'].append(keywordObj)

            articleData.append(ast.literal_eval(json.dumps(articleObj)))
            resultlist[i].append(ast.literal_eval(json.dumps(articleObj)))

        # Return user query (denoted as "uq"), resultlist to result_year.html
        # 'article_with_tag': json.dumps(ast.literal_eval(json.dumps(article_with_tag))), 'cluster_tag': json.dumps(cluster_tag)
        return render_to_response('SearchDB/result_year.html',
                                  {'uq': uq, 'resultlist': resultlist, 'fig': fig, 'xdata_year': xdata,
                                   'ydata_year': ydata, 'abstract': abstract, 'synonym': synonym, 'total': total,
                                   'articleData': articleData,
                                   'article_with_tag': json.dumps(ast.literal_eval(json.dumps(article_with_tag))),
                                   'cluster_tag': json.dumps(cluster_tag),
                                   'wordcloud_data': ast.literal_eval(json.dumps(wordcloud_data))})

    else:
        return render_to_response('SearchDB/search.html')


def refreshDatabase(request):
    # Create a list with filenames in SQL database
    sql_filename = []
    for i in Article.objects.all():
        sql_filename.append(i.filename)

    # Create a list with filenames in local folder
    local_filename = []
    for i in os.listdir(TXT_PATH):
        local_filename.append(i)

    # Create path if it doesn't exist
    if not os.path.exists(TMP_PATH):
        os.mkdir(TMP_PATH)

    # If size changed, refresh dict and mm files
    # Using diff of list for current prototype
    # Using numpy for better performance in the future
    if len(local_filename) != len(sql_filename):

        # Dict
        vector_space_convert(TXT_PATH, TMP_PATH, TMP_PATH, tmpName)
        transformation(TMP_PATH, TMP_PATH, TMP_PATH, tmpName)

        # SQL
        diff_filename = [i for i in local_filename if i not in sql_filename]
        for fname in diff_filename:
            # Load txt file, for content
            f = open(TXT_PATH + fname, 'r')

            # Load pdf file, for title
            pdf_filename = fname.replace(".txt", ".pdf")
            d = doi_extract(PDF_PATH, pdf_filename)  # delete title parameter
            Article.objects.create(
                filename=fname, content=f.read(), doi=d)
            f.close()
    return redirect('/')


def refreshDatabase_Upload():

    # Call this function after uploading files, in order to refresh the database.
    # Create a list with filenames in SQL database
    sql_filename = []
    for i in Article.objects.all():
        sql_filename.append(i.filename)

    # Create a list with filenames in local folder
    # local_filename = []
    # for i in os.listdir(TXT_PATH):
    #     local_filename.append(i)

    # # Create path if it doesn't exist
    # if not os.path.exists(TMP_PATH):
    #     os.mkdir(TMP_PATH)

    # # If size changed, refresh dict and mm files
    # # Using diff of list for current prototype
    # # Using numpy for better performance in the future
    # if len(local_filename) != len(sql_filename):

    #     # Dict
    #     vector_space_convert(TXT_PATH, TMP_PATH, TMP_PATH, tmpName)
    #     transformation(TMP_PATH, TMP_PATH, TMP_PATH, tmpName)

    #     # SQL
    #     diff_filename = [i for i in local_filename if i not in sql_filename]
    #     for fname in diff_filename:
    #         # Load txt file, for content
    #         f = open(TXT_PATH + fname, 'r')

    #         # Load pdf file, for title
    #         pdf_filename = fname.replace(".txt", ".pdf")
    #         d = doi_extract(PDF_PATH, pdf_filename)
    #         Article.objects.create(
    #             filename=fname, content=f.read(), doi=d)  # delete title parameter
    #         f.close()


def linechart(Article_info):
    year_list = []
    for i in range(len(Article_info)):
        year_list.append(Article_info[i][2])
    Max = max(year_list)  # maximum year
    Min = min(year_list)  # minmum year

    xdata = []
    ydata = []
    # crowl through every year between max year and min year
    for i in range(Min, Max + 1, 1):
        xdata.append(i)  # store the year title
        ydata.append(0)  # store the counting of each year
    for i in range(len(Article_info)):
        for j in range(Min, Max + 1, 1):
            if Article_info[i][2] == j:
                ydata[j - Min] += 1
    data = []
    data.append(xdata)
    data.append(ydata)

    # return one big list back to get_text function
    return data


# Chart function
def chart(article_info):
    # Initialize list for counting articles of different percentage
    tmp = [0, 0, 0, 0, 0]
    for element in article_info:
        if element[1] > 0.9:
            tmp[0] += 1
        if 0.8 < element[1] <= 0.9:
            tmp[1] += 1
        if 0.7 < element[1] <= 0.8:
            tmp[2] += 1
        if 0.6 < element[1] <= 0.7:
            tmp[3] += 1
        if 0.5 < element[1] <= 0.6:
            tmp[4] += 1

    # Create an object for the column2d chart using the
    # FusionCharts class constructor
    column2d = FusionCharts("column2d", "ex1", "600", "400", "chart-1", "json",
                            # The data is passed as a
                            # string in the `dataSource` as parameter.
                            {
                                "chart": {
                                    "caption": "Similarity Score distribution",
                                    "xAxisname": "percentage (%)",
                                    "yAxisName": "No. of articles",
                                    "theme": "carbon"
                                },
                                "data": [
                                    {
                                        "label": "91-100",
                                        "value": tmp[0]
                                    },
                                    {
                                        "label": "81-90",
                                        "value": tmp[1]
                                    },
                                    {
                                        "label": "71-80",
                                        "value": tmp[2]
                                    },
                                    {
                                        "label": "61-70",
                                        "value": tmp[3]
                                    },
                                    {
                                        "label": "51-60",
                                        "value": tmp[4]
                                    }
                                ]
                            })

    # Returning complete JavaScript and HTML code, which is used to
    # Generate chart in the browsers.
    return column2d.render()


# Web crawing to import synonym


def getSynonyms(words):
    synonyms = []
    for word in words:
        print(word)
        url = 'http://www.thesaurus.com/browse/' + str(word)
        resp = requests.get(url)
        soup = BeautifulSoup(resp.text, 'html.parser')
        targets = soup.find_all('a', re.compile('css-vdoou0 e1s2bo4t1'))
        print('synonyms:' + str(targets))
        tmp = []
        randomWords = []

        for index, item in enumerate(targets):
            tmp.append(item.text.strip())

        if len(tmp) > 2:
            for _ in range(2):
                index = randint(0, len(tmp) - 1)
                randomWords.append(tmp[index])
        else:
            randomWords = tmp
        synonyms.append(randomWords)
    return synonyms


# for zero uploaded, Don't delete
'''
def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES['file'])
            return HttpResponseRedirect('/success/url/')
    else:
        form = UploadFileForm()
    return render(request, 'Upload.html', {'form': form})
'''


def list(request):
    '''
        while directing to the upload page, the function is called to
        deal with the postprocess to upload files that finally articles can be put into
        the database.

    '''
    newFiles = []
    errorFiles = []
    if not os.path.exists(PDF_TEMP_PATH):
        os.mkdir(PDF_TEMP_PATH)
    if not os.path.exists(TXT_PATH):
        os.mkdir(TXT_PATH)
    if not os.path.exists(ABSTRACT_PATH):
        os.mkdir(ABSTRACT_PATH)
    if not os.path.exists(PDF_PATH):
        os.mkdir(PDF_PATH)
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        files = request.FILES.getlist('docfile')
        if form.is_valid():
            for f in files:
                newdoc = Document(docfile=f)
                newdoc.save()
            # Redirect to the document list after POST
            return HttpResponseRedirect("/Upload/")

    else:
        form = DocumentForm()  # A empty, unbound form

    # Load documents for the list page
    documents = Document.objects.all()
    # for document in documents:
    #     info = getFileMetadata_(document.docfile.path)
    #     # print info
    #     if any(i == '' for i in info):
    #         errorFiles.append(document.docfile.name +
    #                           '\t has no correct metadata')
    #         # print errorFiles #[title,author,year]
    # # postprocess on uploaded file
    # newFiles = postprocessUploadFile()
    # newFiles = newFiles + errorFiles
    # # print newFiles
    # # delete document
    # documents = Document.objects.all().delete()
    # # refresh Database
    # # refreshDatabase_Upload()
    # print(newFiles)
    # Render list page with the documents and the form
    return render(
        request,
        'SearchDB/Upload.html',
        {'documents': newFiles, 'form': form}
    )


def postprocessUploadFile():
    # rename the article
    newFiles = renameFileToPDFTitle(PDF_TEMP_PATH)
    # transfer to txt, and save to txt folder
    newTxt = pdf2txt_(PDF_TEMP_PATH, TXT_PATH, newFiles)
    # move the article to database folder
    moveFile(newFiles, PDF_TEMP_PATH, PDF_PATH)
    # delete U+25A1 characters (white squares) from text files
    conv2utf8(newTxt, TXT_PATH)
    # count the word number in the article
    wordNum = countWordNumber(newTxt, TXT_PATH)
    # Add Flesch reading ease score to text files for analysis
    readingScore = reading_score(newTxt, TXT_PATH)
    # extract abstrct from txt file
    getAbstract(TXT_PATH, ABSTRACT_PATH)
    return newFiles
